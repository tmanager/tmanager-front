tmanager-front
=============

## Prérequis

- `npm`, donc Node.js
- `gulp` : `npm install gulp -g`

## Installation et compilation

- `npm install`. En cas de problèmes de type `EACCESS`, essayer `rm -fr ~/.npm`
- `gulp prod`

## Développement

- `gulp dev`
