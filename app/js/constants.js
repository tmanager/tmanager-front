const AppSettings = {
  appTitle: 'Team Manager',
  baseUrlApi: 'http://team-manager/api'
};

export default AppSettings;
