import angular from 'angular';
import ngResource from 'angular-resource';

var players = angular.module('tmanager.players', [
  'ngResource',
  'ui.router'
]);

players.factory('Players', function(
  $resource,
  AppSettings
) {
  'ngInject';

  return $resource(AppSettings.baseUrlApi + '/players/:playerId', {playerId: '@id'});
});

players.config(function(
  $stateProvider
) {
  'ngInject';
  
  $stateProvider
  .state('players', {
    url: '/players',
    controller: 'PlayerListCtrl',
    templateUrl: 'tmanager/players/list.html',
    title: 'Players'
  })
  ;
});

players.directive('tmPlayer', function() {
  return {
    restrict: 'E',
    scope: {
      player: '=player'
    },
    templateUrl: 'directives/players/player.html'
  }
});

players.controller('PlayerListCtrl', function($scope, Players) {
  'ngInject';
  $scope.players = Players.query();
});
