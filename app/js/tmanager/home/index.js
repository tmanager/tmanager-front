import angular from 'angular';
import config from './config';

var home = angular.module('tmanager.home', []);

home.controller('HomeCtrl', function($scope) {
  'ngInject';

  $scope.welcome = 'This is a sample message';
});

home.config(config);

export default home;
