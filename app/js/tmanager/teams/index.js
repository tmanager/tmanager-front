import angular from 'angular';
import ngResource from 'angular-resource';

var teams = angular.module('tmanager.teams', [
  'ngResource',
  'ui.router'
]);

teams.factory('Teams', function(
  $resource,
  AppSettings
) {
  'ngInject';

  return $resource(AppSettings.baseUrlApi + '/teams/:teamId', {teamId: '@id'});
});

teams.config(function(
  $stateProvider
) {
  'ngInject';
  
  $stateProvider
  .state('teams', {
    url: '/teams',
    controller: 'TeamListCtrl',
    templateUrl: 'tmanager/teams/list.html',
    title: 'Teams'
  })
  ;
});

teams.directive('tmTeam', function() {
  return {
    restrict: 'E',
    scope: {
      player: '=team'
    },
    templateUrl: 'directives/teams/team.html'
  }
});

teams.controller('TeamListCtrl', function($scope, Teams) {
  'ngInject';
  $scope.teams = Teams.query();
});
