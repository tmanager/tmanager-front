import angular from 'angular';

// angular modules
import constants from './constants';
import onConfig  from './on_config';
import onRun     from './on_run';
import 'angular-ui-router';
import './tmanager';
import './templates';

// create and bootstrap tmanagerlication
const requires = [
  'ui.router',
  'templates',
  'tmanager.home',
  'tmanager.players'
];

// mount on window for testing
window.tmanager = angular.module('tmanager', requires);

angular.module('tmanager').constant('AppSettings', constants);

angular.module('tmanager').config(onConfig);

angular.module('tmanager').run(onRun);

angular.bootstrap(document, ['tmanager'], {
  strictDi: true
});
