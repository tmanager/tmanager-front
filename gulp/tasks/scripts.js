'use strict';

import gulp   from 'gulp';
import config from '../config';

gulp.task('scripts', function() {
  return gulp.src(config.js.src)
    .pipe(gulp.dest(config.js.dest))
    ;
});
